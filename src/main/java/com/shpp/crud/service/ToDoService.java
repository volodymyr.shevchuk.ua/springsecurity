package com.shpp.crud.service;

import com.shpp.crud.dto.ToDoDTO;
import com.shpp.crud.model.ToDoEntity;
import com.shpp.crud.repositories.ToDoRepo;
import com.shpp.crud.state.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class ToDoService {


    private final ToDoRepo toDoRepo;

    private final Set<String> namesOfTask = new HashSet<>();

    @Autowired
    public ToDoService(ToDoRepo toDoRepo) {
        this.toDoRepo = toDoRepo;
    }


    public boolean insertIntoDB(ToDoEntity toDoEntity) {
        if (namesOfTask.contains(toDoEntity.getNameOfTask())) {
            return false;
        }
        namesOfTask.add(toDoEntity.getNameOfTask());
        toDoRepo.save(toDoEntity);
        return true;
    }

    public Optional<ToDoEntity> selectByName(String name) {
        return toDoRepo.findFirstByNameOfTask(name);
    }

    public boolean deleteByName(String name) {
        Optional<ToDoEntity> toDoEntity = selectByName(name);
        toDoEntity.ifPresent(toDoRepo::delete);
        return namesOfTask.remove(name);

    }

    public boolean changeStateByName(String name, State state) {
        Optional<ToDoEntity> optional = selectByName(name);
        if (optional.isPresent() && !optional.get().getState().isFinalCondition) {
            toDoRepo.save(optional.get().setState(state));
            return true;
        }
        return false;
    }

    public List<ToDoEntity> selectAll() {
        return toDoRepo.findAll();
    }
}

package com.shpp.crud.state;

public enum State {
    PLANNED(false),
    IN_PROGRESS(false),
    DONE(true),
    CANCELED(true);

    public final boolean isFinalCondition;

    State(boolean b) {
        this.isFinalCondition = b;
    }
}

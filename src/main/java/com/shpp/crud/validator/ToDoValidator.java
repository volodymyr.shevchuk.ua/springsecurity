package com.shpp.crud.validator;

import com.shpp.crud.dto.ToDoDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

public class ToDoValidator implements ConstraintValidator<ToDoConstraint, ToDoDTO> {
    @Override
    public boolean isValid(ToDoDTO toDoDTO, ConstraintValidatorContext constraintValidatorContext) {
        return isTimeValid(toDoDTO);
    }

    private boolean isTimeValid(ToDoDTO toDoDTO){
        return (!toDoDTO.getState().isFinalCondition && toDoDTO.getDeadLine().isAfter(LocalDateTime.now()))
                || (toDoDTO.getState().isFinalCondition && toDoDTO.getDeadLine().isBefore(LocalDateTime.now()));
    }


}

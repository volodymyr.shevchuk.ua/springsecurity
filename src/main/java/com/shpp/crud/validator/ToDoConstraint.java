package com.shpp.crud.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ToDoValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ToDoConstraint {
    String message() default "{nonValidDTO}";
    Class<?>[] groups () default {};
    Class<? extends Payload>[] payload() default {};
}

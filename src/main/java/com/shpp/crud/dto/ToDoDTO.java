package com.shpp.crud.dto;

import com.shpp.crud.model.ToDoEntity;
import com.shpp.crud.state.State;
import com.shpp.crud.validator.ToDoConstraint;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@ToDoConstraint
public class ToDoDTO {

    @NotNull
    private LocalDateTime deadLine;

    @NotNull
    private State state;

    @NotNull
    private String nameOfTask;

    public ToDoDTO() {
    }

    public ToDoEntity convertToEntity(){
        return new ToDoEntity(this.deadLine,this.state,this.nameOfTask);
    }

    public ToDoDTO(LocalDateTime deadLine, State state, String name_of_task) {
        this.deadLine = deadLine;
        this.state = state;
        this.nameOfTask = name_of_task;
    }

    public LocalDateTime getDeadLine() {
        return deadLine;
    }

    public State getState() {
        return state;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public ToDoDTO setDeadLine(LocalDateTime deadLine) {
        this.deadLine = deadLine;
        return this;
    }

    public ToDoDTO setState(State state) {
        this.state = state;
        return this;
    }

    public ToDoDTO setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
        return this;
    }

    @Override
    public String toString() {
        return "ToDoDTO{" +
                "deadLine='" + deadLine + '\'' +
                ", state=" + state +
                ", name_of_task='" + nameOfTask + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDoDTO toDoDTO = (ToDoDTO) o;
        return Objects.equals(deadLine, toDoDTO.deadLine) && state == toDoDTO.state && Objects.equals(nameOfTask, toDoDTO.nameOfTask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deadLine, state, nameOfTask);
    }
}

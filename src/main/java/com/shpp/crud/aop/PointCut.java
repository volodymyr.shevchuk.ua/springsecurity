package com.shpp.crud.aop;

import org.aspectj.lang.annotation.Pointcut;

public class PointCut {

    @Pointcut("execution(* com.shpp.crud.service.ToDoService.*(..))")
    public void servicePointCut(){

    }
    @Pointcut("execution(* com.shpp.crud.controller.ToDoController.*(..))")
    public void controlPointCut(){
    }
}

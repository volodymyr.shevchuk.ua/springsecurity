package com.shpp.crud.aop;

import com.shpp.crud.model.ToDoEntity;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Aspect
@Component
public class LoggAspect {

   private final Logger logger = LoggerFactory.getLogger(LoggAspect.class);

    @Around("PointCut.controlPointCut()")
    public Object loggController(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getName();
        logger.info("method name {} was called",methodName);
        return joinPoint.proceed();
    }

    @Around("PointCut.servicePointCut()")
    public Object loggService(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Object objResult = joinPoint.proceed();
        if (objResult.getClass().isInstance(new ArrayList<>())) {
            return objResult;
        }
        boolean result = (boolean)objResult;
        if(signature.getName().equals("insertIntoDB")){
            if(!result){
                ToDoEntity toDoEntity = (ToDoEntity) Arrays.stream(joinPoint.getArgs()).findFirst().orElse(null);
                logger.error("Object {} is arlready exist",toDoEntity);
            }else{
                logger.info("All ok");
            }
            return result;
        }else if (!result){
            String name = (String)Arrays.stream(joinPoint.getArgs()).findFirst().orElse(null);
            logger.error("Name {} isn't exist",name);
        }
        return result;
    }

}

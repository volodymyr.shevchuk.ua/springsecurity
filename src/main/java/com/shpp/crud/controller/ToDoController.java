package com.shpp.crud.controller;

import com.shpp.crud.dto.ToDoDTO;
import com.shpp.crud.model.ToDoEntity;
import com.shpp.crud.service.ToDoService;
import com.shpp.crud.state.State;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@RestController()
@SecurityRequirement(name = "test_aut")
@RequestMapping("/to-does")
public class ToDoController {


    MessageSource messageSource;
    private final ToDoService toDoService;

    @Autowired
    public ToDoController(ToDoService toDoService, MessageSource messageSource) {
        this.toDoService = toDoService;
        this.messageSource = messageSource;
    }

    @Operation(summary = "get list")
    @GetMapping
    public ResponseEntity<List<ToDoEntity>> getMapper() {
        return ResponseEntity.ok().body(toDoService.selectAll());
    }

    @Operation(summary = "get todo by name")
    @GetMapping("/{name}")
    public ResponseEntity<?> getMapper(@PathVariable String name) {
        return toDoService.selectByName(name).isPresent() ? ResponseEntity.ok().body(toDoService.selectByName(name)) :
                ResponseEntity.notFound().build();
    }

    @PostMapping
    @Operation(summary = "insert todo")
    public ResponseEntity<String> postMapper(@Valid @RequestBody ToDoDTO toDoDTO,
                                             BindingResult bindingResult,
                                             @RequestParam(required = false,defaultValue = "de") Locale locale) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(messageSource.getMessage("unsupportedMessage", null, locale));
        }
        return toDoService.insertIntoDB(toDoDTO.convertToEntity()) ? ResponseEntity.ok()
                .body(messageSource.getMessage("allOk", null, locale))
                : ResponseEntity.badRequest().body(messageSource.getMessage("unsupportedMessage", null, locale));
    }

    @DeleteMapping("/{name}")
    @Operation(summary = "delete todo")
    public ResponseEntity<Void> deleteMapper(@PathVariable String name) {
        return toDoService.deleteByName(name) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @PutMapping("/{name}")
    @Operation(summary = "change todo")
    public ResponseEntity<String> putMapper(@PathVariable String name, @RequestParam State state,
                                            @RequestParam Locale locale) {
        return toDoService.changeStateByName(name, state) ? ResponseEntity.ok().build()
                : ResponseEntity.badRequest().body(messageSource.getMessage("unsupportedMessage", null, locale));
    }

}

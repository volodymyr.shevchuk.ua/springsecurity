package com.shpp.crud.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.shpp.crud.state.State;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity(name = "to_do")
public class ToDoEntity {
    @Id
    @GeneratedValue
    private int ID;

    @NotNull
    private LocalDateTime deadLine;

    @NotNull
    private State state;
    @NotNull
    private String nameOfTask;

    public ToDoEntity() {
    }

    public ToDoEntity(LocalDateTime deadLine, State state, String nameOfTask) {
        this.deadLine = deadLine;
        this.state = state;
        this.nameOfTask = nameOfTask;
    }


    public ToDoEntity setID(int ID) {
        this.ID = ID;
        return this;
    }

    public ToDoEntity setDeadLine(LocalDateTime deadLine) {
        this.deadLine = deadLine;
        return this;
    }

    public ToDoEntity setState(State state) {
        this.state = state;
        return this;
    }

    public ToDoEntity setNameOfTask(String name_of_task) {
        this.nameOfTask = name_of_task;
        return this;
    }

    public int getID() {
        return ID;
    }

    public LocalDateTime getDeadLine() {
        return deadLine;
    }

    public State getState() {
        return state;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "ID=" + ID +
                ", deadLine='" + deadLine + '\'' +
                ", state=" + state +
                ", name_of_task='" + nameOfTask + '\'' +
                '}';
    }

}

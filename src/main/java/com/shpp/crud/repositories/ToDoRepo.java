package com.shpp.crud.repositories;

import com.shpp.crud.model.ToDoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ToDoRepo extends JpaRepository<ToDoEntity,Integer> {
     Optional<ToDoEntity> findFirstByNameOfTask(String name);
}

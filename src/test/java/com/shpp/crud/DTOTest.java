package com.shpp.crud;


import com.shpp.crud.dto.ToDoDTO;
import com.shpp.crud.model.ToDoEntity;
import com.shpp.crud.repositories.ToDoRepo;
import com.shpp.crud.service.ToDoService;
import com.shpp.crud.state.State;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;

public class DTOTest {

    static Validator validator;

    ToDoDTO toDoDTO;

    ToDoRepo toDoRepo = Mockito.mock(ToDoRepo.class);

    ToDoService service = new ToDoService(toDoRepo);


    @BeforeAll
    public static void setUp() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()){
            validator = factory.getValidator();
        }
    }
    @Test
    void TestIsValidTime(){
        toDoDTO = new ToDoDTO(LocalDateTime.now()
                .minus(5, ChronoUnit.MINUTES), State.IN_PROGRESS,"meet with girl");
        Set<ConstraintViolation<ToDoDTO>> constraintViolations = validator.validate(toDoDTO);
        Assertions.assertEquals(1,constraintViolations.size());
    }
    @Test
    void TestStateChanges(){
        Mockito.when(toDoRepo.findFirstByNameOfTask(Mockito.anyString()))
                .thenReturn(Optional.of(new ToDoEntity(LocalDateTime.now(),State.CANCELED,"some task")));
        Assertions.assertFalse(service.changeStateByName("some task", State.IN_PROGRESS));
    }
}

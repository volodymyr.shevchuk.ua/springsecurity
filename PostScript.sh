curl -X POST --location "http://localhost:5000/to-does" -H "Content-Type: application/json" -d "{\"deadLine\": \"2024-01-16T19:17:42.801Z\", \"state\": \"IN_PROGRESS\", \"nameOfTask\": \"go to the dentist\"}" --basic --user user1:kolomuya
echo
curl -X POST --location "http://localhost:5000/to-does" -H "Content-Type: application/json" -d "{\"deadLine\": \"2024-01-16T19:17:42.801Z\", \"state\": \"PLANNED\", \"nameOfTask\": \"repair the car\"}" --basic --user user1:kolomuya
echo
curl -X POST --location "http://localhost:5000/to-does" -H "Content-Type: application/json" -d "{\"deadLine\": \"2023-01-16T19:17:42.801Z\", \"state\": \"CANCELED\", \"nameOfTask\": \"make homework with son\"}" --basic --user user1:kolomuya
echo
curl -X POST --location "http://localhost:5000/to-does" -H "Content-Type: application/json" -d "{\"deadLine\": \"2023-01-16T19:17:42.801Z\", \"state\": \"DONE\", \"nameOfTask\": \"go on the date with wife\"}" --basic --user user1:kolomuya
echo
curl -X POST --location "http://localhost:5000/to-does" -H "Content-Type: application/json" -d "{\"deadLine\": \"2024-01-16T19:17:42.801Z\", \"state\": \"PLANNED\", \"nameOfTask\": \"wash-up car\"}" --basic --user user1:kolomuya
echo